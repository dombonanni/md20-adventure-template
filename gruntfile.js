module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //grunt-beep
    //https://www.npmjs.org/package/grunt-beep
    // It beeps
    beep: {},

    //grunt-autoprefixer
    //
    //Goes to CanIUse and prefixes our properties so we don't have to.
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'android 2.3', 'android 4'],
        map: false,
        diff: '_notes/autoprefixer.diff.css'
      },
      single: {
        src: 'css/<%= pkg.name %>.css',
        dest: 'css/<%= pkg.name %>.css'
      }
    },
    //grunt-contrib-sass
    //https://github.com/gruntjs/grunt-contrib-sass
    //compiles SCSS
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'css/<%= pkg.name %>.css': '_scss/<%= pkg.name %>.scss'
        }
      }
    },

    //grunt-csscomb
    //https://www.npmjs.org/package/grunt-csscomb
    //It organizes CSS properties based on a specified order
    csscomb: {
      options: {
        config: '_scss/.csscomb.json'
      },
      dist: {
        files: {
          //destination : source
          'css/<%= pkg.name %>.css': 'css/<%= pkg.name %>.css',
          //'css/<%= pkg.name %>-theme.css': 'css/<%= pkg.name %>-theme.css'
        }
      }
    },
    concat: {},
    //grunt-contrib-uglify
    //https://github.com/gruntjs/grunt-contrib-uglify
    //It processess and minifies JS
    uglify: {
      options: {
        mangle: true, //shortens and replaces variables names
      },
      my_target: {
        files: {
          'js/<%= pkg.name %>.min.js': ['js/<%= pkg.name %>.js'],
        }
      }
    },
    //grunt-contrib-cssmin
    //https://npmjs.org/package/grunt-contrib-cssmin
    //It minifies CSS
    cssmin: {
      mini: {
        options: {
          banner: '/*! <%= pkg.name %> CSS version <%= pkg.version %> by Cooper Graphic Design */',
          keepSpecialComments: 0,
          report: 'min',
          noAdvanced: true
        },
        files: {
          // 'path/to/destination' : 'path/to/source'
          'css/<%= pkg.name %>.min.css': 'css/<%= pkg.name %>.css'
        }
      }
    },
    htmlmin: { // Task
      dist: { // Target
        options: { // Target options
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          '_deploy/preview/index.html' : 'index.html',
          '_deploy/preview/404.html' : '404.html',
          '_deploy/preview/blog.html' : 'blog.html',
          '_deploy/preview/blog-single.html' : 'blog-single.html',
          '_deploy/preview/work.html' : 'work.html',
          '_deploy/preview/work-single.html' : 'work-single.html',
          '_deploy/preview/about.html' : 'about.html',
          '_deploy/preview/author.html' : 'author.html',

        }
      }
    },
    //grunt-contrib-watch
    //https://github.com/gruntjs/grunt-contrib-watch
    //It fires tasks when files change
    watch: {
      scripts: {
        //path to source scripts
        files: ['js/*.js'],
        tasks: ['uglify', 'beep']
      },
      styles: {
        //path to source styles
        files: ['_scss/*.scss', '_scss/md20/*.scss'],
        tasks: ['dist-css', 'beep']
      }
    }
  });

  // Load the plugins that provide tasks.
  //grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-beep');
  grunt.loadNpmTasks('grunt-csscomb');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  // Register tasks
  // This is what happens when you only type 'grunt'
  grunt.registerTask('default', ['beep']);
  grunt.registerTask('dev-styles', ['watch:styles']);
  grunt.registerTask('dev-scripts', ['watch:scripts']);

  // CSS distribution task.
  //grunt.registerTask('sass', ['less:compileCore', 'less:compileTheme']);
  grunt.registerTask('dist-html', ['htmlmin']);
  grunt.registerTask('dist-css', ['sass', 'autoprefixer', 'csscomb', 'cssmin']);
  grunt.registerTask('splort', ['watch']);
};
