$(document).ready(function() {

  $('.toc a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        $('.toc').toggleClass('open');
        $('.toc').children().fadeToggle();
        return false;
      }
    }
  });
  $('.toc').click(function(event) {
    $(this).toggleClass('open');
    $(this).children().fadeToggle();
  });

  $('.admonition h4, .admonition h5').click(function() {
    $(this).siblings().slideToggle();
  });


});
