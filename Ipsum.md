[TOC]

Generic Adventure
====

Introduction
----

Generic Adventure, authored by Some Guy, is an RPG system-neutral adventure suitable for an indeterminate amount of any level player characters. Player characters who survive the entire adventure might advance in level.

### Reading this Document

> Indented text is read aloud or paraphrased to the players, when appropriate.

Normal text is additional information for the GM. [Useful links](http://www.d20pfsrd.com) are usually included. Sometimes they link outside of the adventure module, and sometimes they move to the [Appendix](#appendix).

````
Statistic blocks are fenced off.
````

### Adventure Background

### Adventure Synopsis

### Character Hooks

### Time of the Year

Highway
----

[Highway][] is a small farming village owned by Erwin Briggs.

> Highway is a small village built on an old plantation, home to about 200 people - mostly humans. The buildings are a mix of brick and timber, most of which are small cottages. The three largest buildings, all situated on one main road, consist of the town hall, the Thin Slug Inn, and the Highwayfarers’ Guildhall. The town generates income by servicing merchants traveling along the coast between the Hills and Rashemen, farms enough produce and raises enough livestock to sustain itself, and generally provides safe haven from the wilderness of the surrounding area. The townsfolk are insular and superstitious, but generally accommodating and law-abiding.

1. **[Specific Location 1][].** Specific description.
2. **[Specific Location 2][].** Specific description.
3. **[Specific Location 3][].** Specific description.


### Location Encounters

#### Specific Location 1
[Specific Location 1]: #specific-location-1

#####Specific Encounter (CR Type)

Note the catalyst for the encounter.

> Enter descriptive player text.

Mark down GM specifics.

#### Specific Location 2
[Specific Location 2]: #specific-location-2

Note the catalyst for the encounter.

> Enter descriptive player text.

Mark down GM specifics.

#### Specific Location 3
[Specific Location 3]: #specific-location-3

Note the catalyst for the encounter.

> Enter descriptive player text.

Mark down GM specifics.

Appendix
----

### NPCs

[Bludjin the Sensible]: #bludjin-the-sensible
!!! block ""
	!!! stats ""
		####Bludjin the Sensible {: .friendly }

		!!! general ""
			**CR 6**
			{: .cr}
			
			**XP 2,400**

			Human fighter 7

			LN Medium humanoid

			**Init** +1;  
			**Perception** +6

		!!! defense ""
			##### Defense

			**AC**
			20,
			touch 11,
			flat-footed 19;
			(+9 armor, +1 Dex)

			**hp** 57 (7d10+19)

			**Fort** +8,
			**Ref** +4,
			**Will** +4;
			+2 vs. fear

			**Defensive Abilities** bravery +2

		!!! offense ""
			##### Offense

			**Speed** 30ft.

			**Melee**
			+1 maul power attack +12/+7 (2d6+14/x3)

			**Ranged**
			composite longbow +8/+3 (1d8+4/x3)

			**Special Attacks**
			power attack (-2 attack / +4 damage)

		!!! statistics ""
			##### Statistics

			**Str** 18,
			**Dex** 12,
			**Con** 14,
			**Int** 13,
			**Wis** 8,
			**Cha** 10

			**Base Atk** +7;
			**CMB** +11;
			**CMD** 22

			**Feats**
			[Alertness](http://www.d20pfsrd.com/feats/general-feats/alertness---final),
			[Combat Expertise](http://www.d20pfsrd.com/feats/combat-feats/combat-expertise-combat),
			[Dazzling Display](http://www.d20pfsrd.com/feats/combat-feats/dazzling-display-combat),
			[Power Attack](http://www.d20pfsrd.com/feats/combat-feats/power-attack-combat---final),
			[Cleave](http://www.d20pfsrd.com/feats/combat-feats/cleave-combat),
			[Iron Will](http://www.d20pfsrd.com/feats/general-feats/iron-will---final),
			[Persuasive](http://www.d20pfsrd.com/feats/general-feats/persuasive---final),
			[Weapon Focus (maul)](http://www.d20pfsrd.com/feats/combat-feats/weapon-focus-combat---final),
			[Weapon Specialization (maul)](http://www.d20pfsrd.com/feats/combat-feats/weapon-specialization-combat---final)

			**Skills**
			Diplomacy +5,
			Handle Animal +4,
			Intimidate +12,
			Knowledge (engineering) +5,
			Perception +6,
			Profession (soldier) +5,
			Ride +2,
			Sense Motive +8

			**Languages**
			Common,
			Halfling

			**SQ**
			armor training 2, weapon training (hammers)

			**Combat Gear**
			potions of [cure moderate wounds](http://www.d20pfsrd.com/magic/all-spells/c/cure-moderate-wounds) (2),
			tanglefoot bags (2)

			**Other Gear**
			masterwork full plate,
			+1 maul,
			composite longbow (+4 Str) with 20 arrows,
			sap,
			[cloak of resistance](http://www.d20pfsrd.com/magic-items/wondrous-items/wondrous-items/c-d/cloak-of-resistance) +1,
			key to Highwayfarers' Guildhall,
			key to Highwayfarers' Guildhall vault,
			key to Tipton Bank deposit box,
			35gp

		!!! strategy ""
			##### Strategy

			When pressed into combat, in the first round Bludjin uses his Dazzling Display feat to demoralize all opponents in the area and calls for aide. In the second round, he closes distance and starts mauling people.

			If dropped below 10hp, Bludjin will surrender.

		!!! description ""
			##### Description

			He's huge.

			##### Boon

			Bludjin can rally a posse in 8 hours, bringing together a group of 2d4 low-level warriors to aid in one specific plan.


### Monsters

[Larval Carrion Crawler]: #larval-carrion-crawler
!!! block ""
	!!! stats ""
		####Larval Carrion Crawler {: .hostile }

		!!! general ""
			**CR 1**
			{: .cr}
			
			**XP 400**

			N Medium aberration

			**Init** +2;
			**Senses** darkvision 60ft., [scent](http://www.d20pfsrd.com/gamemastering/special-abilities#TOC-Scent);
			**Perception** +8

		!!! defense ""
			##### Defense

			**AC**
			15,
			touch 12,
			flat-footed 13;
			(+2 Dex, +3 natural)

			**hp** 5 (1d8+1)

			**Fort** +1,
			**Ref** +2,
			**Will** +3

		!!! offense ""
			##### Offense

			**Speed** 20ft., climb 10ft.

			**Melee**
			tentacles +1 (1d3+1 plus [grab](http://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Grab-Ex-)) and
			bite +1 (1d4+1)

			**Special Attacks**
			[constrict](http://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Constrict-Ex-) (1d3+1)

			**Space** 5ft.;
			**Reach** 5ft.

		!!! statistics ""
			##### Statistics

			**Str** 12,
			**Dex** 14,
			**Con** 12,
			**Int** 1,
			**Wis** 12,
			**Cha** 6

			**Base Atk** +0;
			**CMB** +1 (+5 grapple);
			**CMD** 13 (can't be tripped)

			**Feats**
			Skill Focus (Perception)
			
			**Skills**
			Climb +9,
			Escape Artist +8,
			Perception +8

			**Languages**
			none

			**SQ**
			slippery

		!!! special-abilities ""
			##### Special Abilities

			**Slippery (Ex)**
			A slime crawler exudes a thin, oily film from the glands beneath its mouth that leaves a slug-like trail behind it as its moves and provides the creature with a +6 racial bonus to Escape Artist checks. A creature stepping in a space covered with this slime must succeed on a DC 11 Reflex save or slip and fall prone. The slime remains in the area for 1d2 hours before losing its potency. The save DC is Constitution-based.

		!!! strategy ""
			##### Strategy

			Immediately flees to Mature Carrion Crawlers. Failing that, it hides. Combat is a last resort.

			A larval slime crawler attacks with its tentacles, attempting to grab an opponent and squeeze it. Slime crawlers have a nasty bite, but prefer to use their tentacles in battle.

		!!! ecology ""
			##### Ecology

			**Environment** any land and underground

			**Organization** solitrary, cluster (2-5), swarm (5-10), or nest (10-20)

			**Treasure** none

		!!! description ""
			##### Description

			This creature appears to be a segmented worm or caterpillar with stumpy, almost nonexistent legs. Four tentacles sprout below its mouth. The creature's body is covered in an oily film that leaves a slime path behind it as it moves.

			Larval slime crawlers usually mature into this form within two to three weeks of hatching, at which time they feed on any living organisms encountered. More slug-like at this larval stage, the legs appear as small buds or stumps. These legs allow the slime crawler to climb walls and other surfaces, albeit slower than a mature slime crawler. Four tentacles sprout below its throat, eventually losing their grappling ability and growing into the pseudopod-like tentacles of the mature slime crawler.

			A typical larval slime crawler is about 6 feet long and weighs about 300 pounds.

### Traps and Hazards

[Vampiric Touch Trap]: #vampiric-touch-trap
!!! block ""
	!!! stats ""
		####Vampiric Touch Trap {: .hostile }

		!!! general ""
			**CR 7**
			{: .cr}
			
			**XP 3,200**

			**Type** magic (CL 12);
			**Perception** DC 28;
			**Disable Device** DC 28

		!!! effects ""
			#####Effects

			**Trigger** touch ([alarm](http://www.d20pfsrd.com/magic/all-spells/a/alarm)); 
			**Reset** automatic (eight hours from trigger)

			**Effect**
			spell effect ([vampiric touch](http://www.d20pfsrd.com/magic/all-spells/v/vampiric-touch), 6d6 damage, no save); single target (living creature who touches door)

### City Statistics

[Highway]: #highway-village
!!! block ""
	!!! stats ""
		####Highway Village

		!!! general ""
			N village

			**Corruption** -1;
			**Crime** -6;
			**Economy** -0;
			**Law** +3;
			**Lore** 0;
			**Society** +1

			**Qualities** insular, superstitious [(reference)](http://www.d20pfsrd.com/gamemastering/other-rules/settlements) 

			**Danger** +1

		!!! economy ""
			#####Economy

			**Base Value** 500gp;
			**Purchase Limit** 2,500gp;
			**Spellcasting** 1st

		!!! magic ""
			#####Magic

			**Minor Items** 2d4;

			**Medium Items** 1d4;

		!!! demographics ""
			#####Demographics

			**Government** autocracy

			**Population** 176 (168 human, 7 dwarf, 1 gnome)

		!!! npcs ""
			#####Notable NPCs

			**Highwayfarers Guild Leader** [Bludjin the Sensible][]

			**Mayor** Erwin Briggs (LN male human commoner 5)

<link rel="stylesheet" href="css/md20-adventure-template.min.css" type="text/css" media="all">
<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/md20.js"></script>